USE [master]
GO
/****** Object:  Database [DrawingDB]    Script Date: 13/02/2020 10:16:16 ******/
CREATE DATABASE [DrawingDB]

USE [DrawingDB]
GO
/****** Object:  Table [dbo].[imageTBL]    Script Date: 13/02/2020 10:16:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[imageTBL](
	[imageId] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](30) NULL,
	[description] [varchar](200) NULL,
	[pictuer] [nvarchar](200) NULL,
 CONSTRAINT [PK__imageTBL__336E9B5569DF8691] PRIMARY KEY CLUSTERED 
(
	[imageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[shapeImageTBL]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[shapeImageTBL](
	[shapeImageId] [int] IDENTITY(1,1) NOT NULL,
	[userImageId] [int] NULL,
	[shapeId] [int] NULL,
	[centerX] [float] NULL,
	[centerY] [float] NULL,
	[width] [float] NULL,
	[height] [float] NULL,
	[color] [varchar](30) NULL,
	[fillingColor] [varchar](30) NULL,
	[opacity] [float] NULL,
	[angle] [float] NULL,
	[shapeTxt] [varchar](100) NULL,
	[startX] [float] NULL,
	[startY] [float] NULL,
	[endX] [float] NULL,
	[endY] [float] NULL,
 CONSTRAINT [PK__shapeIma__76E4C2C4658CBD8E] PRIMARY KEY CLUSTERED 
(
	[shapeImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[shapeTBL]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[shapeTBL](
	[shapeId] [int] IDENTITY(1,1) NOT NULL,
	[shapeName] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[shapeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sharedTBL]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sharedTBL](
	[sharedID] [int] IDENTITY(1,1) NOT NULL,
	[userImageId] [int] NULL,
	[userId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[sharedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[userImageTBL]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userImageTBL](
	[userImageId] [int] IDENTITY(1,1) NOT NULL,
	[imageId] [int] NULL,
	[userId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[userImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[userTBL]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userTBL](
	[userId] [int] IDENTITY(1,1) NOT NULL,
	[lastName] [varchar](30) NULL,
	[firstName] [varchar](30) NULL,
	[password] [varchar](50) NULL,
	[userName] [nchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[imageTBL] ON 
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (7, N'hihi', N'hi', N'http://localhost:53524/Images/לאבא.jpg')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (8, N'hihi', N'hi', N'http://localhost:53524/Images/לאבא.jpg')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (9, N'hihi', N'hi', N'http://localhost:53524/Images/בלונים-כחול-לבן.jpg')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (10, N'hihi', N'hi', N'http://localhost:53524/Images/לאמא1.jpeg')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (11, N'hihi', N'hi', N'http://localhost:53524/Images/בלונים3.jpg')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (12, N'hihi', N'hi', N'http://localhost:53524/Images/לאבא.jpg')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (13, N'hihi', N'hi', N'http://localhost:53524/Images/בלונים2.jpg')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (14, N'hihi', N'hi', N'http://localhost:53524/Images/לאמא1.jpeg')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (15, N'hihi', N'hi', N'http://localhost:53524/Images/לאמא.jpg')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (16, N'hihi', N'hi', N'http://localhost:53524/Images/Inkedmail_LI.jpg')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (17, N'hihi', N'hi', N'http://localhost:53524/Images/דפי-צביעה-מיני-מאוס-1.gif')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (18, N'hihi', N'hi', N'http://localhost:53524/Images/44.jpg')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (19, N'hihi', N'hi', N'http://localhost:53524/Images/33.png')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (20, N'שלום', N'שלום', N'http://localhost:53524/Images/22.png')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (21, N'ב', N'ב', N'http://localhost:53524/Images/Inkedmail_LI.jpg')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (22, N'ע', N'ע', N'http://localhost:53524/Images/44.jpg')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (23, N'j', N'j', N'http://localhost:53524/Images/1.jpg')
GO
INSERT [dbo].[imageTBL] ([imageId], [name], [description], [pictuer]) VALUES (24, N'nb n', N'bvnbvn', N'http://localhost:53524/Images/לאמא.jpg')
GO
SET IDENTITY_INSERT [dbo].[imageTBL] OFF
GO
SET IDENTITY_INSERT [dbo].[shapeImageTBL] ON 
GO
INSERT [dbo].[shapeImageTBL] ([shapeImageId], [userImageId], [shapeId], [centerX], [centerY], [width], [height], [color], [fillingColor], [opacity], [angle], [shapeTxt], [startX], [startY], [endX], [endY]) VALUES (29, 39, 1, 323.00416056315106, 336.79583485921222, 92.5, 85.5, N'black', N'#00ff00', 0.84, 0, N'', 0, 0, 0, 0)
GO
INSERT [dbo].[shapeImageTBL] ([shapeImageId], [userImageId], [shapeId], [centerX], [centerY], [width], [height], [color], [fillingColor], [opacity], [angle], [shapeTxt], [startX], [startY], [endX], [endY]) VALUES (30, 39, 1, 323.00416056315106, 336.79583485921222, 92.5, 85.5, N'black', N'#00ff00', 0.84, 0, N'', 0, 0, 0, 0)
GO
INSERT [dbo].[shapeImageTBL] ([shapeImageId], [userImageId], [shapeId], [centerX], [centerY], [width], [height], [color], [fillingColor], [opacity], [angle], [shapeTxt], [startX], [startY], [endX], [endY]) VALUES (31, 39, 2, 0, 0, 43.5, 75.5, N'black', N'#ffff00', 1, 0, N'', 549.02499389648438, 210.15000152587891, 0, 0)
GO
INSERT [dbo].[shapeImageTBL] ([shapeImageId], [userImageId], [shapeId], [centerX], [centerY], [width], [height], [color], [fillingColor], [opacity], [angle], [shapeTxt], [startX], [startY], [endX], [endY]) VALUES (32, 39, 1, 323.00416056315106, 336.79583485921222, 92.5, 85.5, N'black', N'#00ff00', 0.84, 0, N'', 0, 0, 0, 0)
GO
INSERT [dbo].[shapeImageTBL] ([shapeImageId], [userImageId], [shapeId], [centerX], [centerY], [width], [height], [color], [fillingColor], [opacity], [angle], [shapeTxt], [startX], [startY], [endX], [endY]) VALUES (33, 39, 1, 323.00416056315106, 336.79583485921222, 92.5, 85.5, N'black', N'#00ff00', 0.84, 0, N'', 0, 0, 0, 0)
GO
INSERT [dbo].[shapeImageTBL] ([shapeImageId], [userImageId], [shapeId], [centerX], [centerY], [width], [height], [color], [fillingColor], [opacity], [angle], [shapeTxt], [startX], [startY], [endX], [endY]) VALUES (34, 39, 2, 0, 0, 43.5, 75.5, N'black', N'#ffff00', 1, 0, N'', 549.02499389648438, 210.15000152587891, 0, 0)
GO
INSERT [dbo].[shapeImageTBL] ([shapeImageId], [userImageId], [shapeId], [centerX], [centerY], [width], [height], [color], [fillingColor], [opacity], [angle], [shapeTxt], [startX], [startY], [endX], [endY]) VALUES (35, 39, 1, 373.72196359345406, 166.28939241351503, 40.5, 64, N'black', N'#ff80c0', 1, 0, N'', 0, 0, 0, 0)
GO
SET IDENTITY_INSERT [dbo].[shapeImageTBL] OFF
GO
SET IDENTITY_INSERT [dbo].[shapeTBL] ON 
GO
INSERT [dbo].[shapeTBL] ([shapeId], [shapeName]) VALUES (1, N'עיגול')
GO
INSERT [dbo].[shapeTBL] ([shapeId], [shapeName]) VALUES (2, N'ריבוע')
GO
INSERT [dbo].[shapeTBL] ([shapeId], [shapeName]) VALUES (3, N'קו')
GO
SET IDENTITY_INSERT [dbo].[shapeTBL] OFF
GO
SET IDENTITY_INSERT [dbo].[userImageTBL] ON 
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (33, 7, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (34, 8, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (35, 9, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (36, 10, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (37, 11, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (38, 12, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (39, 13, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (40, 14, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (41, 15, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (42, 16, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (43, 17, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (44, 18, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (45, 19, 13)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (46, 20, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (47, 21, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (48, 22, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (49, 23, 1)
GO
INSERT [dbo].[userImageTBL] ([userImageId], [imageId], [userId]) VALUES (50, 24, 1)
GO
SET IDENTITY_INSERT [dbo].[userImageTBL] OFF
GO
SET IDENTITY_INSERT [dbo].[userTBL] ON 
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (1, N'זיכל', N'ברכה', N'123456', N'brachi                        ')
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (2, N'zhfk', N'ברכה', N'12345', N'fdshgf                        ')
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (3, N'zhfk', N'ברכה', N'12345', N'fdshgf                        ')
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (4, N'zhfk', N'ברכה', N'12345', N'fdshgf                        ')
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (5, N'zhfk', N'ברכה', N'12345', N'fdshgf                        ')
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (6, N'NעכגעגULL', N'עכגד', N'123456', N'brachi                        ')
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (7, N'zhfk', N'ברכה', N'12345', N'fdshgf                        ')
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (8, N'zhfk', N'ברכה', N'12345', N'fdshgf                        ')
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (9, N'zhfk', N'ברכה', N'12345', N'fdshgf                        ')
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (10, N'zhfk', N'ברכה', N'12345', N'fdshgf                        ')
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (11, N'zhfk', N'ברכה', N'12345', N'fdshgf                        ')
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (12, N'לודמיר', N'ברכה', N'123456', N'brachiLudmir                  ')
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (13, N'אלישיב', N'מיכל', N'123456', N'michal                        ')
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (14, N'דסי', N'מנדלסון', N'123456', N'dasi                          ')
GO
INSERT [dbo].[userTBL] ([userId], [lastName], [firstName], [password], [userName]) VALUES (15, N'לודמיר', N'ברכה', N'125', N'brachi                        ')
GO
SET IDENTITY_INSERT [dbo].[userTBL] OFF
GO
ALTER TABLE [dbo].[shapeImageTBL]  WITH CHECK ADD  CONSTRAINT [FK__shapeImag__shape__1ED998B2] FOREIGN KEY([shapeId])
REFERENCES [dbo].[shapeTBL] ([shapeId])
GO
ALTER TABLE [dbo].[shapeImageTBL] CHECK CONSTRAINT [FK__shapeImag__shape__1ED998B2]
GO
ALTER TABLE [dbo].[shapeImageTBL]  WITH CHECK ADD  CONSTRAINT [FK__shapeImag__userI__1DE57479] FOREIGN KEY([userImageId])
REFERENCES [dbo].[userImageTBL] ([userImageId])
GO
ALTER TABLE [dbo].[shapeImageTBL] CHECK CONSTRAINT [FK__shapeImag__userI__1DE57479]
GO
ALTER TABLE [dbo].[sharedTBL]  WITH CHECK ADD FOREIGN KEY([userImageId])
REFERENCES [dbo].[userImageTBL] ([userImageId])
GO
ALTER TABLE [dbo].[sharedTBL]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[userTBL] ([userId])
GO
ALTER TABLE [dbo].[userImageTBL]  WITH CHECK ADD  CONSTRAINT [FK__userImage__image__44FF419A] FOREIGN KEY([imageId])
REFERENCES [dbo].[imageTBL] ([imageId])
GO
ALTER TABLE [dbo].[userImageTBL] CHECK CONSTRAINT [FK__userImage__image__44FF419A]
GO
ALTER TABLE [dbo].[userImageTBL]  WITH CHECK ADD FOREIGN KEY([userId])
REFERENCES [dbo].[userTBL] ([userId])
GO
/****** Object:  StoredProcedure [dbo].[addImage]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[addImage]
@name varchar(30), @description varchar(200), @picture nvarchar(200)
as
begin
INSERT INTO [dbo].[imageTBL] ([name],[description],[pictuer])
values (@name,@description,@picture)
end
GO
/****** Object:  StoredProcedure [dbo].[addImageByUser]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[addImageByUser]
@name varchar(30), @description varchar(200), @picture nvarchar(200), @userId int
as
begin
exec addImage @name,@description,@picture
INSERT INTO [dbo].[userImageTBL] ([imageId],[userId])
values (IDENT_CURRENT('imageTBL'),@userId)
end
GO
/****** Object:  StoredProcedure [dbo].[addShapeToImage]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[addShapeToImage] 
@userImageId int, @shapeId int, 
@centerX float=0, @centerY float=0, @width float=0, @height float=0,@color varchar(30)='red', @fillingcolor varchar(30)='black', @opacity float=1, 
@angle float=0,  @shapeTxt varchar(100)='', 
@startX float=0, @startY float=0, @endX float=0, @endY float=0
as
begin
INSERT INTO [dbo].[shapeImageTBL] ([userImageId],[shapeId],[centerX],[centerY],[width],[height],
[color],[fillingColor],[opacity],[angle],[shapeTxt],[startX],[startY],[endX],[endY])
values (@userImageId, @shapeId, @centerX, @centerY, @width, @height,@color, @fillingcolor, @opacity,
@angle, @shapeTxt,  @startX, @startY, @endX, @endY)
exec getShapesByImage @userImageId
end
GO
/****** Object:  StoredProcedure [dbo].[addSharedToImage]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[addSharedToImage] 
@userImageId int, @shareId int
as
begin
insert into [dbo].[sharedTBL] ([userImageId],[userId])
values (@userImageId,@shareId)
exec getSharedsByImage @userImageId
end
GO
/****** Object:  StoredProcedure [dbo].[addUser]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[addUser] 
@lName varchar(30),@fName varchar(30),@password varchar(50),@userName varchar(30)
as
begin
INSERT INTO [dbo].[userTBL] ([lastName],[firstName],[password],[userName])
values (@lName, @fName, @password,@userName)
end

GO
/****** Object:  StoredProcedure [dbo].[checkUser]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[checkUser] (@userName varchar(30),@password varchar(50))
as
begin
select * from [dbo].[userTBL] where [userName]=@userName and [password]=@password
end
GO
/****** Object:  StoredProcedure [dbo].[deleteShapeFromeImage]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[deleteShapeFromeImage] 
@shapeImageId int
as
begin
DELETE FROM [dbo].[shapeImageTBL] where
[shapeImageId]=@shapeImageId
declare @userImageId int
set @userImageId =(select [userImageId] from [dbo].[shapeImageTBL] where [shapeImageId]=@shapeImageId)
exec getShapesByImage @userImageId
end
GO
/****** Object:  StoredProcedure [dbo].[deleteSharedFromeImage]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[deleteSharedFromeImage] 
@sharedId int
as
begin
DELETE FROM [dbo].[sharedTBL]
where [sharedID]=@sharedId
declare @userImageId int
set @userImageId =(select [userImageId] from [dbo].[sharedTBL] where [sharedID]=@sharedId)
exec getShapesByImage @userImageId
end
GO
/****** Object:  StoredProcedure [dbo].[editShapeInImage]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[editShapeInImage] 
@shapeImageId int, @shapeId int, 
@centerX float=null, @centerY float=null, @width float=null, @height float=null, @fillingcolor varchar(30)=null, @opacity float=null, 
@color varchar(30)=null, @shapeTxt varchar(100)=null, @angle float=null, 
@startX float=null, @startY float=null, @endX float=null, @endY float=null
as
begin
Update [dbo].[shapeImageTBL]
set
[shapeId]=@shapeId, [centerX]=@centerX,
[cenrerY]=@centerY, [width]=@width,
[height]=@height, [color]=@color,
[fillingColor]=@fillingcolor, [opacity]=@opacity,
[angle]=@angle, [shapeTxt]=@shapeTxt,
[startX]=@startX, [startY]=@startY,
[endX]=@endX, [endY]=@endY
where [shapeImageId]=@shapeImageId
declare @userImageId int
set @userImageId =(select [userImageId] from [dbo].[shapeImageTBL] where [shapeImageId]=@shapeImageId)
exec getShapesByImage @userImageId
end
GO
/****** Object:  StoredProcedure [dbo].[getAllMyImages]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[getAllMyImages]
(@userId int)
as
begin
select userImageTBL.userImageId,userImageTBL.[imageId],[name],[description],[pictuer],userImageTBL.[userId]
from [dbo].[userImageTBL] left join [dbo].[sharedTBL]
on [userImageTBL].userImageId=[sharedTBL].userImageId
join [dbo].[imageTBL] on [dbo].[imageTBL].[imageId]=[dbo].[userImageTBL].[imageId]
join [dbo].[userTBL] on [dbo].[userImageTBL].[userId]=[userTBL].[userId]
where [dbo].[sharedTBL].[userId]=@userId or [userImageTBL].userId=@userId
end
GO
/****** Object:  StoredProcedure [dbo].[getAllShapes]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[getAllShapes]
as
begin
select * from [dbo].[shapeTBL]
end
GO
/****** Object:  StoredProcedure [dbo].[getAllUsers]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[getAllUsers]
as
begin
select * from [dbo].[userTBL]
end
GO
/****** Object:  StoredProcedure [dbo].[getImageByShared]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[getImageByShared](@shareId int)
as
begin
select userImageTBL.userImageId,userName,[name],[description],[pictuer]
from [dbo].[userImageTBL] join [dbo].[sharedTBL]
on [userImageTBL].userImageId=[sharedTBL].userImageId
and [dbo].[sharedTBL].[userId]=@shareId
join [dbo].[imageTBL] on [dbo].[imageTBL].[imageId]=[dbo].[userImageTBL].[imageId]
join [dbo].[userTBL] on [dbo].[userImageTBL].[userId]=[userTBL].[userId]
end
GO
/****** Object:  StoredProcedure [dbo].[getImageByUser]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[getImageByUser](@userId int)
as
begin
select [userImageId],[dbo].[userImageTBL].[imageId],[name],[description],[pictuer],[userId] from [dbo].[imageTBL] 
join [dbo].[userImageTBL] on [dbo].[imageTBL].[imageId]=[dbo].[userImageTBL].[imageId]
where userId=@userId
end
GO
/****** Object:  StoredProcedure [dbo].[getShapesByImage]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[getShapesByImage] 
@userImageId int
as
begin
select * from [dbo].[shapeImageTBL]
where userImageId=@userImageId
end
GO
/****** Object:  StoredProcedure [dbo].[getSharedsByImage]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[getSharedsByImage]
@userImageId int
as
begin
select [sharedID],[userImageId],[dbo].[sharedTBL].userId,[lastName],[firstName] from [dbo].[userTBL] 
join [dbo].[sharedTBL] on [dbo].[userTBL].userId=[dbo].[sharedTBL].userId
and userImageId=@userImageId
end
GO
/****** Object:  StoredProcedure [dbo].[searchImagesByCriterions]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[searchImagesByCriterions] 
@userName varchar(30)=null, @sharedName varchar(30)=null, 
@imageName varchar(30)=null, @imgDescription varchar(200)=null,
@shapeName varchar(20)=null, @fillingcolor varchar(30)=null, @color varchar(30)=null, @shapeTxt varchar(100)=null
as
begin
select  [userImageTBL].[userImageId],[imageName],[imgDescription],[pictuer] from [dbo].[imageTBL]
join [dbo].[userImageTBL] on [imageTBL].imageId=[userImageTBL].imageId
join [dbo].[userTBL] on [userImageTBL].userId=[userTBL].userId
left join [dbo].[shapeImageTBL] on [userImageTBL].userImageId=[shapeImageTBL].userImageId
left join [dbo].[shapeTBL] on [shapeImageTBL].shapeId=[shapeTBL].shapeId
left join [dbo].[sharedTBL] on [userImageTBL].userImageId=[sharedTBL].userImageId
left join [dbo].[userTBL] shareds on [sharedTBL].userId=shareds.userId
where (@userName is null or userTBL.[userName] =@userName)
and (@sharedName is null or shareds.[userName]=@sharedName)
and (@imageName is null or [imageName]=@imageName)
and (@imgDescription is null or [imgDescription]=@imgDescription)
and (@shapeName is null or [shapeName]=@shapeName)
and (@fillingcolor is null or [fillingColor]=@fillingcolor)
and (@color is null or [color]=@color)
and (@shapeTxt is null or [shapeTxt]=@shapeTxt)
OPTION (RECOMPILE)
end

GO
/****** Object:  StoredProcedure [dbo].[tryOut]    Script Date: 13/02/2020 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[tryOut] (@userName varchar(30),@password varchar(50))
as
begin
select [userId] from [dbo].[userTBL] where [userName]=@userName and [password]=@password
end
GO
USE [master]
GO
ALTER DATABASE [DrawingDB] SET  READ_WRITE 
GO
